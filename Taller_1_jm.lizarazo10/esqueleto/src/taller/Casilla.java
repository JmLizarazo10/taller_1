
package taller;

/**
 * Clase que representa una casilla del triqui
 */
public class Casilla
{

    // -----------------------------------------------------------------
    // Constantes
    // -----------------------------------------------------------------

    /**
     * Indica cuando la casilla est� vacia.
     */
    final public static String VACIA = "";

    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------

    /**
     * Marca que cada jugador tiene.
     */
    private String marca;

    // -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------
    /**
     * Se crean las casillas sin marcar "Vacias".
     */
    public Casilla( )
    {
        marca = VACIA;
    }

    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------

    /**
     * Indica que la casilla la seleccion� un jugador y se le asigna su marca a la casilla. <br>
     * <b>PRE: </b> El Triqui se debio crear e inicializar. <br>
     * <b>POS: </b> Se asign� la marca a la casilla actual. <br>
     * @param pMarca Marca que se asignar� a la casilla actual. pmarca != null y pmarca != "".
     */
    public void asignarMarca( String pMarca )
    {
        marca = pMarca;
    }

    /**
     * Metodo que devuelve la marca que tiene la casilla. <br>
     * <b>pre: </b> El Triqui se debio crear e inicializar. <br>
     * @return marca Marca de la casilla seleccionada. marca != null.
     */
    public String obtenerMarca( )
    {
        return marca;
    }

    /**
     * Metodo que indica si la casilla est� vac�a. <br>
     * <b>pre: </b> El Triqui se debio crear e inicializar. <br>
     * @return verdadero cuando la casilla esta vac�a. De lo contrario, falso.
     */
    public boolean estaVacia( )
    {
        boolean vacia = false;
        if( marca.equals( VACIA ) )
            vacia = true;
        return vacia;
    }

    /**
     * Borra la marca asignada a la casilla. <br>
     * <b>pre: </b> El Triqui se debio crear e inicializar. <br>
     * <b>post: </b> Se pone vacia la marca. Sin nada (No null). <br>
     */
    public void limpiar( )
    {
        marca = "";
        
    }

    /**
     * Revisa y responde si la marca hecha por el jugador es la misma que entra como parametro <br>
     * <b>pre: </b>El Triqui se debio crear e inicializar. <br>
     * @param pMarca Marca que se va a comparar. pMarca != null.
     * @return verdadero si la marca corresponde a la que es y devuelve falso en caso contrario.
     */
    public boolean marcada( String pMarca )
    {
        boolean marcada = marca.equals( pMarca );
        return marcada;
    }
}