package taller;

/**
 * Tablero del Triqui
 */
public class Triqui
{
    // -----------------------------------------------------------------
    // Constantes
    // -----------------------------------------------------------------
    /**
     * Matriz de posici�n de casillas
     */
  public final static int[] CASILLAS = {1,2,3,4,5,6,7,8,9};



    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------

    /**
     * Matriz de casillas
     */
public Casilla
[] casillas;

public Casilla casilla;
    // -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------

    /**
     * Crea el tablero del triqui preparando todas las casillas
     */
public Triqui( )
{
 casillas =  new Casilla[9];
 {
  for (int i = 0; i < 9; i++)
  {
    casillas[i] = new Casilla();
   
  }
 }
}


    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------

    /**
     * Metodo que marca  una de las casillas del tablero. Las casillas se numeran del 1 al 9 empezando por la fila superior y de izquierda a derecha. <br>
     * <b>POS: </b> La casilla que entra por parametro es marcada <br>
     * @param casilla N�mero de la casilla. 1 <= casilla <= 9
     * @param marca Marca que se le hace a la casilla. marca != null y marca != "".
     */
    public void marcarCasilla( int casilla, String pmarca ) throws Exception
    {
     for (int i = 0; i < 9; i++)
     {
       if (CASILLAS[i] == casilla)
       {

        if (casillas[i].estaVacia())
         {
        casillas[i].asignarMarca(pmarca);
      }
        else
        {
         throw new Exception("La casilla ya est� coupada");
        }
       }
      
     }
    }

    /**
     * Metodo que retorna la marca asignada a una casilla del tablero. <br>
     * @param casilla N�mero de la casilla. 1 <= casilla <= 9.
     * @return marca Marca de la casilla.
     */
    public String obtenerMarcaCasilla( int casilla )
    {
     String rta = Casilla.VACIA;
     for (int i = 0; i < 9; i++)
     {
       if (CASILLAS[i] == casilla)
       {
        rta = casillas[i].obtenerMarca();
       }
      }
     return rta;
     }


    /**
     * Metodo que indica si la diagonal principal (de la casilla superior izquierda a la inferior derecha) est� llena para un jugador. <br>
     * @param marcaJugador Marca de alg�n jugador. marcaJugador != null.
     * @return verdadero si todas las casillas de la diagonal tienen la marca del jugador, falso en caso contrario.
     */
    public boolean diagonalPrincipalLlena( String marcaJugador )
    {
     boolean contiene = false;


      if (casillas[0].marcada( marcaJugador ) && casillas[4].marcada( marcaJugador ) && casillas[8].marcada( marcaJugador )  )
      {
       contiene = true;
      }
     
     return contiene;
    }

    /**
     * Indica si la diagonal secundaria (de la casilla superior derecha a la inferior izquierda) est� llena para un jugador. <br>
     * @param marcaJugador Marca de alg�n jugador. marcaJugador != null.
     * @return true si todas las casillas de la diagonal tienen la marca del jugador, falso en caso contrario.
     */
    public boolean diagonalSecundariaLlena( String marcaJugador )
    {
        boolean contiene = false;

      if (casillas[6].marcada( marcaJugador ) && casillas[4].marcada( marcaJugador ) && casillas[2].marcada( marcaJugador ))
      {
       contiene = true;
      }

        return contiene;
    }

    /**
     * Indica si la fila superior est� llena para un jugador. <br>
     * @param marcaJugador Marca de alg�n jugador. marcaJugador != null.
     * @return verdadero si todas las casillas de la fila tienen la marca del jugador, falso en caso contrario.
     */
    public boolean filaLlena( String marcaJugador )
    {
        boolean contiene = false;
     for (int i = 0; i < 3; i++)
     {

      if ((casillas[0].marcada( marcaJugador ) && casillas[1].marcada( marcaJugador ) && casillas[2].marcada( marcaJugador ))||(casillas[3].marcada( marcaJugador ) && casillas[4].marcada( marcaJugador ) && casillas[5].marcada( marcaJugador ))||(casillas[6].marcada( marcaJugador ) && casillas[7].marcada( marcaJugador ) && casillas[8].marcada( marcaJugador )))
      {
       contiene = true;
      }
     }

        return contiene;
    }


    /**
     * Metodo que indica si la columna izquierda est� llena para un jugador. <br>
     * @param marcaJugador Marca de alg�n jugador. marcaJugador != null.
     * @return verdadero si todas las casillas de la columna tienen la marca del jugador, falso en caso contrario.
     */
    public boolean columnaLlena( String marcaJugador )
    {
        boolean contiene = false;
     for (int i = 0; i < 3; i++)
     {

      if ((casillas[0].marcada( marcaJugador ) && casillas[3].marcada( marcaJugador ) && casillas[6].marcada( marcaJugador ))||(casillas[1].marcada( marcaJugador ) && casillas[4].marcada( marcaJugador ) && casillas[7].marcada( marcaJugador ))||(casillas[2].marcada( marcaJugador ) && casillas[5].marcada( marcaJugador ) && casillas[8].marcada( marcaJugador )))
      {
       contiene = true;
      }
     }

        return contiene;
    }

  

    /**
     * Metodo que indica si una casilla est� vac�a o no. Las casillas se numeran del 1 al 9 empezando por la fila superior y de izquierda a derecha. <br>
     * @param casilla N�mero que representa la casilla. 1 < casilla < 9.
     * @return verdadero si la casilla est� vac�a, falso si no lo est�.
     */
    public boolean casillaVacia( int casilla )
    {
     boolean rta = false;

     for (int i = 0; i < 9; i++)
     {
       if (CASILLAS[i] == casilla)
       {
        rta = casillas[i].estaVacia();
       }
     }
     return rta;
    }
    /**
     * Metodo que  informa si el tablero est� lleno, es decir que tiene todas las casillas marcadas. <br>
     * @return verdadero si el tablero est� lleno, falso si no lo est�.
     */
    public boolean tableroLleno( )
    {
        boolean estaLleno = ! ( casillas[0].estaVacia( ) || casillas[1].estaVacia( ) || casillas[2].estaVacia( ) || casillas[3].estaVacia( ) || casillas[4].estaVacia( ) || casillas[5].estaVacia( ) || casillas[6].estaVacia( ) || casillas[7].estaVacia( ) || casillas[8].estaVacia( ) );
        return estaLleno;
    }
    
    public boolean tableroVacio(){
        boolean estaVacio =  ( casillas[0].estaVacia( ) || casillas[1].estaVacia( ) || casillas[2].estaVacia( ) || casillas[3].estaVacia( ) || casillas[4].estaVacia( ) || casillas[5].estaVacia( ) || casillas[6].estaVacia( ) || casillas[7].estaVacia( ) || casillas[8].estaVacia( ) );
        return estaVacio;
    }

    /**
     * Metodo que indica si un jugador (seg�n su marca) gan� el juego. Gana quien complete las casillas de alguna diagonal o fila o columna con su marca. <br>
     * @param marca Marca del jugador actual, marca != null y marca != "".
     * @return true si el jugador actual gan� el juego, false en caso contrario.
     */
    public boolean gana( String marca )
    {
        boolean gano = ( diagonalPrincipalLlena( marca ) || diagonalSecundariaLlena( marca ) || filaLlena( marca ) || columnaLlena( marca ) );
        return gano;

    }

    /**
     * Metodo que limpia todas las marcas en las casillas del tablero. <br>
     * <b>post: </b> Se limpian todas las casillas de marcas. <br>
     */
    public void limpiarTablero( )
    {
     for (int i = 0; i < 9; i++)

       casillas[i].limpiar();

      }
    }