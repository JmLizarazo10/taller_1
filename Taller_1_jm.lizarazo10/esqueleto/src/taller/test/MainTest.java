package taller.test;
import junit.framework.TestCase;
import taller.*;

public class MainTest extends TestCase {
 
 Casilla uno = new Casilla();
 InterfazTriqui p;
 Triqui triqui = new Triqui();
 
    private void setupEscenario1( )
    {
     p = new InterfazTriqui();
     p.juego();

    }
 
 
 public void testMarcarCasillaOcupada()
 {
  setupEscenario1();
  boolean rta;
  try{
  p.jugada(1);
  rta = p.jugada(1);
  }
  catch(Exception e){
   rta = false; 
  }
  assertFalse("Esta bien", rta); 
  
 }
 
 public void TestTerminoJuego(){
   setupEscenario1();
   try{
   p.jugada(1);
   p.jugada(2);
   p.jugada(4);
   p.jugada(6);
   p.jugada(7);
   }
   catch(Exception e){
     assertTrue(false);
   }
   assertTrue(triqui.gana("O"));
 }
 
 public void TestVerificarTableroLimpio(){
   setupEscenario1();
   assertTrue(triqui.tableroVacio());
 }

}