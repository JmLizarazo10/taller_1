package taller;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;




/**
 * Interfaz gr�fica del juego del triqui
 */
public class InterfazTriqui extends JFrame
{
   // -----------------------------------------------------------------
    // Constantes
    // -----------------------------------------------------------------
    /**
     * Marca para el jugador humano
     */
    private final static String JUGADOR1 = "X";

    /**
     * Marca para el jugador PC
     */
    private final static String JUGADOR2 = "O";

    // -----------------------------------------------------------------
    // Atributos de Interfaz
    // -----------------------------------------------------------------
    /**
     * Panel triqui
     */
 
    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------
    /**
     * Juego de triqui
     */
    private Triqui juegoTriqui;
    /**
     * Contador de los turnos
     */
    int contador = 0;
    
    /**
     * Scanner de consola
     */
    private Scanner sc;
    /**
     * Se inicializa el arreglo con las jugadas
     */
    private String[] casillas;

    private boolean finJuego;
    // -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------

    /**
     * Construye la interfaz del triqui
     * @param titulo Nombre que tendr� el Frame que se despliega. t�tulo != null.
     */
    public InterfazTriqui()
    {
        // Juego Triqui
        juegoTriqui = new Triqui( );
        crearTriqui();
        
        // Jugador PC del Triqui
        sc = new Scanner(System.in);
        int rta = sc.nextInt();
       seleccionMenu(rta);
         

    }

    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------

    public void seleccionMenu(int rta)
    {
     if (rta == 0)
     {
         System.out.println("Iniciando Juego...");
         System.out.println("Comienza el jugador 1 con O");
         finJuego= false;
      do{
       juego();
      }while(!finJuego);
         System.out.println("Para jugar de nuevo, oprimir 0. Para simular un juego, marque -1");

     }
     if (rta==-1)
     {
      do{
      simulacion();
      }while(!finJuego);
         System.out.println("Para jugar una nueva partida, oprimir 0. Para simular ude nuevo, marque -1");
     }
     if(rta != 0 || rta != -1)
      rta = sc.nextInt();
      seleccionMenu(rta);

    }
    /**
     * Metodo que inicializa y a�ade todas las casillas.
     */
    private void crearTriqui( )
    {
        casillas = new String[9];

        for( int i = 0; i < 9; i++ )
        {
         casillas[i]= new String("");
        }
    }
    
    
    public void juego()
    {
     int casilla = sc.nextInt();
     if (casilla >0  && casilla < 10)
      try
     {
      jugada(casilla);
     }
     catch(Exception e)
     {
   System.out.println(e.getMessage());
     }
     else if (casilla == 0)
     {
      juegoTriqui.limpiarTablero();
  System.out.println("");
  System.out.println("/***************************************************/");
  System.out.println("Iniciando nuevo juego...");
     }
  else
      System.out.println("Ingrese un numero valido entre 0 y 9");

    }
  
    /**
     * Metodo que valida y ejecuta la jugada de alguno de los dos jugadores
     * @param casillaJugada Casilla en la que jug� el jugador actual. casillaJugada != null o ""
     */
    public boolean jugada(int casilla) throws Exception
    {
       
        // Se se�ala en que casilla y quien jug�
        
         if ( contador == 0 || (contador%2 == 0))
         {
          try
          {
         juegoTriqui.marcarCasilla( casilla, JUGADOR2 );
      
            //marca la jugada en el Triqui.
            marcarTriqui( );
            // Aumenta la cuenta de jugadas.
            contador();
            System.out.println("El jugador 1 ha jugado en la casilla " + casilla);

          }
          catch (Exception e)
          {
           throw e;
          }

         }
         else
         {
          try
          {
         juegoTriqui.marcarCasilla( casilla, JUGADOR1 );
            //marca la jugada en el Triqui.
            marcarTriqui( );
            // Aumenta la cuenta de jugadas.
            contador();
            System.out.println("El jugador 2 ha jugado en la casilla " + casilla);
          }
          catch (Exception e)
          {
           throw e;
          }


            }
         
         
            //Dice quien gana el juego o si no hay ganador
         
         if( juegoTriqui.gana( JUGADOR1 ) )
         {
          System.out.println("");
            System.out.println("Felicitaciones. El jugador 2 ha ganado y el jugador 1 ha perdido.");
            finJuego = true;
          juegoTriqui.limpiarTablero();
          System.out.println("");
          System.out.println("/***************************************************/");
           }

         else if( juegoTriqui.gana( JUGADOR2) )
         {
            System.out.println("");
            System.out.println("Felicitaciones. El jugador 1 ha ganado y el jugador 2 ha perdido.");
            finJuego = true;
          juegoTriqui.limpiarTablero();
          System.out.println("");
          System.out.println("/***************************************************/");

         }
         else if(juegoTriqui.tableroLleno())
         {
            System.out.println("");
            System.out.println("�Empate!");
            finJuego = true;
            juegoTriqui.limpiarTablero();
          System.out.println("");
          System.out.println("/**********************************************************/");
         }
         return false;
    }
          

    /**
     * Metodo que muestra las jugadas hechas durante un juego.
     */
    public void marcarTriqui( )
    {

        String marca;
        

        for( int i = 0; i < 9; i++ )
        {
            if( !juegoTriqui.casillaVacia( i +1) )
            {
                marca = juegoTriqui.obtenerMarcaCasilla( i +1);
                casillas[i] =marca;             
            }
            else
             casillas[i]= " ";
            
            if (i==2 || i==5)
            {
            System.out.println("|"+ casillas[i]);
            System.out.println("-+-+-");
            }
            else if ( i==8)
                System.out.println("|"+ casillas[i]);

            
            else if (i==0 || i==3 || i==6)
            System.out.print(casillas[i]+"|" );
            else
                System.out.print(casillas[i]);

        }
    }
    
    /**
     * Metodo que cuenta las jugadas hechas en el juego.
     * <b> POS: <b/> Suma uno cada turno jugado.
     * 
     */
    
    public void contador()
    {
     contador++;
    }
    
    public void simulacion()
    {
     int numero = (int)(Math.random()*10+1);
     if(numero <10)
      try
     {
     jugada(numero);
     }
     catch (Exception e)
     {
      
     }
    }
    /**
     * M�todo principal de ejecuci�n
     * @param args 
     */
    public static void main( String[] args )
    {
        System.out.println("Bienvenidos a jugar Triqui");
        System.out.println("Instrucciones:");
        System.out.println(" �El rpimer jugador inicia con O, el segundo jugar� con X.");
        System.out.println(" �Para jugar, cada jugador debe escribir el numero correspondiente a la casilla que desee jugar.");
        System.out.println("  Asi:");
        System.out.println("   1|2|3");
        System.out.println("   -+-+-");
        System.out.println("   4|5|6");
        System.out.println("   -+-+-");
        System.out.println("   7|8|9");

        System.out.println(" �Gana el primer jugador que logre hacer una linea horizontal, vertical o diagonal  con 3 Xs u Os seguidas (Seg�n sea el caso). De lo contrario habr� un empate!");
        System.out.println(" �Si desea iniciar un nuevo juego marque 0.");
        System.out.println(" �Si desea iniciar una nueva simulaci�n de la maquina paso por paso, ingrese -1.");

        System.out.println("");
        
        InterfazTriqui x = new InterfazTriqui();
        

    }
    
}